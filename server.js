const _ = require('lodash')
const assert = require('assert')
const generate = require('./generate.js')
const {
    Client,
    GatewayIntentBits,
    SlashCommandBuilder,
    Routes,
} = require('discord.js')
const {REST} = require('@discordjs/rest')
const env = process.env
const token = 'MTAwNjk3MjU5NTA1ODUyMDExNA.G62Qq7.ekS2nMu_EuuN1Zmpu8W5zCfExBDXXIMq-PQGRI'
let models = (env.MODELS||'').split(' ')
assert(models, 'Missing MODEL_PATH')

let main = async ()=>{
    let client = new Client({intents: [GatewayIntentBits.Guilds]})
    client.on('interactionCreate', async i=>{
        let prompt = i.options.get('prompt')?.value
        if (!prompt)
            return i.reply(`You didn't send a prompt`)
        let limit = i.options.get('length')?.value||100
        console.log('[prompt] (user=%s, length=%d) "%s"', i.user.username,
            limit, _.truncate(prompt, 30))
        await i.deferReply()
        let reply = generate.continue(prompt, {model: models[0], limit})
        i.editReply(reply.replace(prompt, t=>{
            return `**${prompt}**`
        }))
    })
    client.once('ready', c=>{
        console.log(`Logged in`, c.user.username)
    })
    await client.login(token)
    for (let [id, guild] of await client.guilds.fetch())
        await init_guild(client, guild)
}

let init_guild = async (client, guild)=>{
    log('[%s] init guild', guild.id)
    let rest = new REST({version: '10'}).setToken(token)
    let commands_url =
        Routes.applicationGuildCommands(client.application.id, guild.id);
    log('[%s] init command /story', guild.id)
    await rest.put(commands_url, {
        body: [
            new SlashCommandBuilder()
            .setName('story')
            .setDescription('Tell me a story')
            .addStringOption(o=>o.setName('prompt')
                .setRequired(true)
                .setDescription('Get me started'))
            .addIntegerOption(o=>o.setName('length')
                .setDescription('How long should the story be?')
                .setMinValue(1)
                .setMaxValue(1000)),
        ].map(v=>v.toJSON()),
    })
}

let log = (...args)=>console.log(...args)

main().catch(e=>{
    console.error(e)
    process.exit(1)
})
