let E = module.exports

E.build = ([size, ...rest], fn)=>{
    if (!rest.length)
        return [...new Array(size)].map(()=>fn())
    return [...new Array(size)].map(()=>E.build(rest, fn))
}
