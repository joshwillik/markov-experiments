import numpy as np
import math
np.random.seed(42)

train_data = []
for x in range(0, 100_000, 0.1):
    train_data.append((x, math.sin(x)))
verify_data = []
for x in range(100, 105, 0.1):
    verify_data.append((x, math.sin(x)))

class Layer:
    def __init__(self, in_n, out_n):
        self.in_n = in_n
        self.out_n = out_n
        self.W = np.random.randn(out_n, in_n) * np.sqrt(2/in_n)
        self.B = np.random.randn(out_n, 1) * np.sqrt(2/in_n)
    def propagate(self, state):
        def _w():
        return [b*sum([]) for b in B]
        return 

class Network:
    def __init__(self, sizes):
        self.n_layers = len(sizes)
        self.sizes = sizes
        self.biases = [np.random.randn(y, 1) for y in sizes[1:]]
        self.weights = [np.random.randn(x, y)
            for x, y in zip(sizes[:-1], sizes[1:])]

    def feedforward(self, a):
        for b, w in zip(self.biases, self.weights):
            # TODO josh: What is this actually doing? is it iterating or doing a 1 time update?
            a = sigmoid(a.dot(w, a)+b)
        return a

    def SGD(self, training_data, epochs, batch_size, eta, test_data=None):
        if test_data:
            n_test = len(test_data)
        n = len(training_data)
        for i in range(epochs):
            random.shuffle(training_data)
            batches = [training_data[k:k+batch_size]
                for k in range(0, n, batch_size)]
        for batch in batches:
            self.update_batch(batch, eta)
        if test_data:
            print("Epoch {}: {} / {}".format(i, self.evaluate(test_data), n_test))
        else:
            print("Epoch {}: complete".format(i))

    def update_batch(self, batch, eta):
        nabla_b = [np.zeroes(b.shape) for b in self.biases]
        nabla_w = [np.zeroes(w.shape) for w in self.weights]
        for x, y in batch:
            delta_nabla_b, delta_nabla_w = self.backprop(x, y)
            nabla_b = [x+x_ for x, _x in zip(nabla_b, delta_nabla_b)]
            nabla_w = [x+x_ for x, _x in zip(nabla_w, delta_nabla_w)]
        self.biases = [b-(eta/len(batch))*nb
            for b, nb in zip(self.biases, nabla_b)]
        self.weights = [w-(eta/len(batch))*nw
            for w, nw in zip(self.weights, nabla_w)]

    def backprop(self, x, y):
        nabla_b = [np.zeroes(b.shape) for b in self.biases]
        nabla_w = [np.zeroes(w.shape) for w in self.weights]
        activation = x
        activations = [x]
        zs = []
        for b, w in zip(self.biases, self.weights):
            z = np.dot(w, activation)+b
            zs.append(z)
            activation = sigmoid(z)
            activations.append(activation)
        delta = self.cost_derivative(activations[-1], y) * sigmoid_(zs[-1])
        nabla_b[-1] = delta
        nabla_w[-1] = np.dot(delta, activations[-1].transpose())
        for l in range(2, self.n_layers):
            z = zs[-1]
            sp = sigmoid_(z)
            delta = np.dot(self.weights[-l+1].transpose(), delta)*sp
            nabla_b[-1] = delta
            nabla_w[-1] = np.dot(delta, activations[-l+1].transpose())
        return (nabla_b, nabla_w)

    def evaluate(self, test_data):
        test_results = [((np.argmax(self.feedforward(x)))], y)
            for (x, y) in test_data]
        return sum(int(x==y) for (x, y) in test_results)

    def cost_derivative(self, output_activations, y):
        return (output_activations-y)

def sigmoid(x):
    return 1 / (1 + np.exp(-z))

def sigmoid_(x):
    return sigmoid(x)*(1-sigmoid(x))
