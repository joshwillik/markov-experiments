const assert = require('assert')
const fs = require('fs')
const bytes  = require('bytes')
const text = require('./text.js')
const {Neural_network} = require('./model.js')

let layers = [100, 50]
let main = ()=>{
    let [node, self, ...texts] = process.argv
    let usage = `Usage: node ${self} <...texts>`
    assert(texts, usage)
    let alphabet = new Set()
    for (let f of texts)
    {
        for (let c of fs.readFileSync(f).toString())
            alphabet.add(c)
    }
    alphabet = [...alphabet]
    console.log(alphabet)
    process.exit(0)
    let model = new Neural_network({
        layers: [
            ...layers,
            alphabet.length,
        ],
        alphabet,
    })
    let tokens = 0
    for (let f of texts)
    {
        feed_text(model, f, {
            on_learn(){
                tokens++
                if (tokens%1000==0)
                {
                    let mem = process.memoryUsage()
                    console.error('Learned %d samples', tokens, bytes(mem.heapTotal))
                }
            }
        })
    }
    console.log(JSON.stringify(model.serialize()))
};
let feed_text = (model, f, {on_learn})=>{
    let book = fs.readFileSync(f).toString()
    for (let [str, next] of text.windows(book, 100))
    {
        model.learn(str, next)
        on_learn()
    }
};

if (!module.parent)
    main();
