const _ = require('lodash')
const assert = require('assert')
const fs = require('fs')
const {Markov_model} = require('./model.js')
const parser = require('./parser.js')
let E = exports;

let max = map=>{
    let choice = Object.entries(map).reduce((a, b)=>{
        if (a[1]>b[1])
            return a;
        return b;
    });
    return choice[0];
};
let weighted = map=>{
    return weighted_choice(Object.keys(map), Object.values(map));
};
let weighted_choice = (options, weights)=>{
    let sum_weight = weights.reduce((a, b)=>a+b, 1);
    let relative = weights.map(w=>w/sum_weight);
    let scale = [];
    weights.reduce((sum, w)=>{
        scale.push(sum+w);
        return sum+w;
    }, 0);
    let r = Math.random();
    let i = scale.findIndex(w=>r<=w);
    return options[i];
};
let select = weighted;
let load_model = _.memoize(file=>{
    return Markov_model.from(JSON.parse(fs.readFileSync(file)));
})
let invert = _.memoize(o=>{
    let out = {};
    for (let [k, v] of Object.entries(o))
        out[v] = k;
    return out;
});
E.continue = (text, {model, limit})=>{
    let {memory, states: S, weights: W} = load_model(model)
    let _S = invert(S);
    let key = arr=>arr.join(',');
    let state = 0, state_key = [], out_text = [];
    let first_tokens = Array.from(parser.parse(text));
    for (let i = 0; i<limit; i++)
    {
        let token = i in first_tokens ? first_tokens[i] : S[state];
        out_text.push(token);
        state = _S[token];
        if (state==undefined)
            state_key = [];
        else
            state_key = [...state_key, state].slice(-memory);
        while (state_key.length && !W[state_key.join(',')])
            state_key.shift();
        let next = W[state_key.join(',')];
        if (next)
            state = select(next);
        else
        {
            if  (i>=first_tokens.length-1)
            {
                out_text.push('... Where was I? Uh...');
                state = Math.floor(Math.random()*S.length);
                state_key = [];
            }
        }
    }
    return out_text.join(' ');
}
if (!module.parent)
{
    let [node, self, model_file, words, prompt] = process.argv
    words = +words;
    let usage = 'Usage: node generate.js <model_file> <words>';
    assert(model_file, usage);
    assert(words>=0, usage);
    console.log(E.continue(prompt||"", {model: model_file, limit: words}))
}
