const list = require('./list.js')
const vec = require('./vec.js')
let E = exports;
E.Markov_model = class Markov_model {
    #states;
    #weights;
    state = [];
    constructor({
        memory,
        states,
        weights,
    }){
        this.memory = memory;
        this.#states = states||new Map();
        this.#weights = weights||{};
        this.counter = Array.from(this.#states.keys()).length;
    }
    get states(){ return Array.from(this.#states.keys()); }
    get weights(){ return this.#weights; }
    learn(token){
        let token_i = this.index(token);
        for (let i = 0; i<this.state.length; i++)
            this.learn_transition(this.state.slice(-i), this.index(token));
        this.state = [...this.state, token_i].slice(-this.memory);
    }
    index(token){
        if (!this.#states.has(token))
            this.#states.set(token, this.counter++);
        return this.#states.get(token);
    }
    learn_transition(state, to_state){
        let key = state.join(',');
        if (!this.#weights[key])
            this.#weights[key] = {};
        this.#weights[key][to_state] = (this.#weights[key][to_state]||0)+1;
    }
    normalize_weights(){
        for (let [k, weights] of Object.entries(this.#weights))
        {
            let sum = Object.values(weights).reduce((a, b)=>a+b, 0);
            let _weights = Object.entries(weights).reduce((o2, [k2, v])=>{
                o2[k2] = v/sum;
                return o2;
            }, {});
            this.#weights[k] = _weights;
        }
    }
    serialize(){
        return {
            memory: this.memory,
            states: Array.from(this.#states.keys()),
            weights: Object.entries(this.#weights),
        }
    }
    static from(serialized){
        let {memory, states, weights} = serialized;
        return new Markov_model({
            memory,
            states: states.reduce((o, k, i)=>{
                o.set(k, i);
                return o;
            }, new Map()),
            weights: Object.fromEntries(weights),
        });
    }
}
E.Neural_network = class {
    #alphabet
    #layers
    #weights
    #biases
    #_layers
    constructor({layers, alphabet, weights, biases}){
        this.#alphabet = alphabet
        this.#layers = layers
        this.#weights = weights || build_weights(layers)
        this.#biases = biases || build_biases(layers)
    }
    train(input, output){
        let _output = this.from_alphabet(output)
        let guess = this.predict_raw(input)
        let diff = vec.sub(_output, guess)
    }
    predict(input){ return this.to_alphabet(this.predict_raw(input)) }
    predict_raw(input){
        let state = input
        for (let [w, b] of list.zip(this.#weights, this.#biases))
            state = vec.apply(sigmoid, vec.add(vec.dot(state, w), b))
        return state
    }
}

const build_weights = layers=>{
    let out = []
    for (let [a, b] of list.zip(layers, layers.slice(1)))
        out.push(vec.build([a, b], ()=>Math.random()))
    return out
}

const build_biases = layers=>{
    let out = []
    for (let l of layers)
        out.push(new Array(l).map(()=>Math.random()))
    return out
}

if (!module.parent)
    console.log('B', build_biases([4, 6, 4]))
