const assert = require('assert')
const fs = require('fs')
const bytes  = require('bytes')
const parser = require('./parser.js')
const {Markov_model} = require('./model.js')

let main = ()=>{
    let [node, self, memory, ...texts] = process.argv
    memory = +memory
    let usage = `Usage: node ${self} <memory> <...texts>`
    assert(texts, usage)
    assert(memory>=0, usage)
    let model = new Markov_model({memory})
    let tokens = 0
    for (let f of texts)
    {
        feed_text(model, f, {
            on_learn(){
                tokens++
                if (tokens%1000==0)
                {
                    let mem = process.memoryUsage()
                    console.error('Learned %d tokens (mem_used=%s)',
                        tokens, bytes(mem.heapTotal))
                }
            }
        })
    }
    model.normalize_weights();
    console.log(JSON.stringify(model.serialize()))
};
let feed_text = (model, f, {on_learn})=>{
    let book = fs.readFileSync(f).toString()
    for (let token of parser.parse(book))
    {
        model.learn(token)
        on_learn()
    }
};

if (!module.parent)
    main();
