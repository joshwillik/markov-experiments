from pathlib import Path
from typing import List, Tuple
import torch
import pdb
import datetime
import json
from itertools import chain
from more_itertools import chunked, windowed
from torch import nn, optim
import torch.nn.functional as F
from torch.utils.data import Dataset as T_dataset
from torch.utils.data import DataLoader
import numpy as np
from collections import Counter
from multiprocessing import Pool

DO_CUDA = True
MODEL_PATH = Path("models/baseline")
MODEL_PATH.mkdir(exist_ok=True)
sequence_length = 4
overfit_prompt = 'Most of us have some family members like this'[0:sequence_length]

class Ticker():
    def __init__(self, rollover, leading=False):
        first = 0
        if leading:
            first = rollover-1
        self.v = first
        self.counter = first
        self.rollover = rollover 
    def inc(self):
        self.v += 1
        self.counter = (self.counter+1) % self.rollover
        return self.rollover!=-1 and self.counter==0
    def value(self):
        return self.v

class Model(nn.Module):
    def __init__(self, dataset, hyper):
        super(Model, self).__init__()
        self.hyper = hyper
        self.num_layers = 1
        self.lstm = nn.LSTM(
            input_size=dataset.n_uniq_tokens,
            hidden_size=hyper['lstm_size'],
            num_layers=self.num_layers,
            batch_first=True,
        )
        self.fc = nn.Linear(hyper['lstm_size'], dataset.n_uniq_tokens)
    def forward(self, x, prev_state):
        output, state = self.lstm(x, prev_state)
        # CONTINUE: passing the full LSTM state to the linear layer doesn't
        # make much sense, right? Try removing the sequence_length-1 first
        # dimensions to see if this makes training produce sensible results

        logits = self.fc(output[-1, :])
        return logits, state, 
    def init_state(self, sequence_length):
        hyper = self.hyper
        return (
            torch.zeros(self.num_layers, hyper['lstm_size']),
            torch.zeros(self.num_layers, hyper['lstm_size']),
        )

def parse_jsonl(lines):
    data = []
    _l = ""
    try:
        for l in lines:
            _l = l
            data.append(json.loads(l))
    except Exception as e:
        print('FAILED TO PARSE', _l, lines)
        raise e
    return data

def _windowed(*args):
    for w in windowed(*args):
        yield [x for x in w if x is not None]

class Dataset(T_dataset):
    def __init__(self, hyper):
        self.hyper = hyper
        self.raw_samples = self.load_input()[0:50]
        print('samples', self.raw_samples[0:10])
        self.uniq_tokens = self.get_uniq_tokens()
        self.n_uniq_tokens = len(self.uniq_tokens)
        self.index_to_token = {index: token for index, token in enumerate(self.uniq_tokens)}
        self.token_to_index = {token: index for index, token in enumerate(self.uniq_tokens)}
        self.samples = self.normalize_samples()
    def load_input(self):
        lines = []
        print(f'Loading dataset')
        with open('../datasets/reddit_comments/sample.jsonl') as f:
            lines = f.readlines()
        with Pool(10) as p:
            samples = list(chain(*p.map(parse_jsonl, chunked(lines, 10000))))
            return [x["body"] for x in samples]
    def get_uniq_tokens(self):
        tokens = set()
        for s in self.raw_samples:
            tokens.update(set(s))
        return tokens
    def normalize_samples(self) -> List[Tuple[torch.Tensor, torch.Tensor]]:
        n_samples = len(self.raw_samples)
        print(f'Preprocessing dataset 0/{n_samples}')
        tick = Ticker(100)
        samples = []
        n_tokens = self.hyper['sequence_length']
        for s in self.raw_samples:
            if tick.inc():
                print(f'Preprocessing dataset: {tick.value()}/{n_samples}')
            samples.append([
                (
                    self.normalize(w[0:-1]),
                    F.one_hot(torch.tensor(self.token_to_index[w[-1]]),
                              self.n_uniq_tokens).float()
                )
                for w in _windowed(s, n_tokens+1)
            ])
        return samples
    def normalize(self, sample):
        padding = max(0, self.hyper['sequence_length']-len(sample))
        return torch.cat((
            torch.zeros(padding, self.n_uniq_tokens),
            torch.stack([
                F.one_hot(torch.tensor(self.token_to_index[x]),
                    self.n_uniq_tokens).float()
                for x in sample
            ])
        ))[-self.hyper['sequence_length']:]
    def denormalize(self, t):
        return ''.join([self.index_to_token[x] for x in t.argmax(1).tolist()])
    def __len__(self):
        return len(self.samples)
    def __getitem__(self, index):
        return self.samples[index]

class Avg():
    def __init__(self):
        self.count = 0.0
        self.average = 0.0
    def value(self):
        return self.average
    def add(self, v):
        self.average = (self.count*self.average + v)/(self.count+1)
        self.count += 1

def train(dataset, model, hyper, start_epoch=0, start_batch=0):
    if DO_CUDA:
        model.cuda()
    model.train()
    print(f'Dataset has {dataset.n_uniq_tokens} classes')
    dataloader = DataLoader(dataset, batch_size=hyper['batch_size'])
    loss_fn = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr=0.001)
    sample_ticker = Ticker(200)
    test_ticker = Ticker(5)
    save_ticker = Ticker(5)
    loss = None
    for epoch in range(start_epoch, hyper['max_epochs']):
        loss_avg = Avg()
        print(f'EPOCH {epoch}')
        for batch, sample in enumerate(dataset):
            state_h, state_c = model.init_state(hyper['sequence_length'])
            if DO_CUDA:
                state_h = state_h.cuda()
                state_c = state_c.cuda()
            for x, y in sample:
                if DO_CUDA:
                    x = x.cuda()
                    y = y.cuda()
                optimizer.zero_grad()
                y_guess, (state_h, state_c) = model(x, (state_h, state_c))
                loss = loss_fn(y_guess, y)
                loss_avg.add(loss.item())
                state_h = state_h.detach()
                state_c = state_c.detach()
                loss.backward()
                optimizer.step()
                if sample_ticker.inc():
                    print({'epoch': epoch, 'batch': batch, 'sample_loss': loss.item()})
        if test_ticker.inc():
            print(predict(model, dataset, overfit_prompt, 80))
            model.train()
        if save_ticker.inc():
            print(f'Saving {epoch}.{batch}')
            timestamp = datetime.datetime.timestamp(datetime.datetime.now())*1000
            torch.save(model.state_dict(),
                MODEL_PATH.joinpath(f'{epoch}.{batch}.{timestamp}.json'))
        print(f'EPOCH {epoch} loss: {loss_avg.value()}')
        if loss_avg.value()<0.002:
            print('Loss crossed training threshold, stopping')
            break
    print(f'DONE TRAINING {loss.item()}')
    print(predict(model, dataset, overfit_prompt, 200))
    print(predict(model, dataset, 'How many', 200))
    print(predict(model, dataset, 'The upside', 200))
    print(predict(model, dataset, 'hey bb', 200))

def predict(model, dataset, text, next_words):
    model.eval()
    sequence_length = model.hyper['sequence_length']
    words = dataset.normalize(text)
    state_h, state_c = model.init_state(len(words))
    if DO_CUDA:
        state_h = state_h.cuda()
        state_c = state_c.cuda()
    for i in range(0, next_words):
        x = words[-sequence_length:]
        if DO_CUDA:
            x = x.cuda()
        y_guess, (state_h, state_c) = model(x, (state_h, state_c))
        p = F.softmax(y_guess, dim=0).detach().cpu().numpy()
        index = np.random.choice(len(y_guess), p=p)
        index = y_guess.argmax(0).item()
        words = torch.cat((
            words,
            F.one_hot(torch.tensor(index), dataset.n_uniq_tokens).float().unsqueeze(0)
        ))
    return text+dataset.denormalize(words)[-next_words:]

def main():
    hyper = {
        'sequence_length': sequence_length, # 4,
        'lstm_size': 512, # 256, # 128,
        'batch_size': 128,
        'max_epochs': 50,
    }
    dataset = Dataset(hyper)
    model = Model(dataset, hyper)
    checkpoint_version = '0.0'
    checkpoint_path = None
    epoch = 0
    batch = 0
    for f in Path(MODEL_PATH).iterdir():
        (epoch, batch, *rest) = f.name.split('.')
        epoch = int(epoch)
        batch = int(batch)
        version = f'{epoch}.{batch}'
        if checkpoint_version<version:
            checkpoint_version = version
            checkpoint_path = f
    if checkpoint_path is not None:
        print(f'Loading checkpoint: {f}')
        model.load_state_dict(torch.load(checkpoint_path))
        model.eval()
    else:
        print(f'No checkpoint found, starting from scratch')
    train(dataset, model, hyper, epoch, batch)

if __name__=='__main__':
    main()
