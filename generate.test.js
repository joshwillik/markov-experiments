const os = require('os')
const fs = require('fs/promises')
const _test = require('ava')
const {Model} = require('./model.js')
const generate = require('./generate.js')

let tmp_dir = `${os.tmpdir()}/generate_test.${Date.now()}`
let test = (name, fn)=>_test(name, async ()=>{
    await fs.mkdir(tmp_dir)
    try { await fn(); }
    finally { await fs.rm(tmp_dir, {recursive: true}) }
});

test('generate', t=>{
    let txt = generate.continue('', {});
    txt.is(txt, '');
});
