let E = exports;

E.parse = function*parse(text){
    let eat_token = i=>{
        let end_i = (str, i, match)=>{
            for (; i<=str.length; i++)
            {
                if (match(str[i]))
                    return i;
            }
            return str.length;
        };
        let is_whitespace = c=>' \n\t'.includes(c);
        let match, type;
        if (is_whitespace(text[i]))
        {
            match = c=>!is_whitespace(c);
            type = 'whitespace';
        }
        else
        {
            match = c=>is_whitespace(c);
            type = 'text';
        }
        return [type, [i, end_i(text, i, match)]];
    }
    let i = 0;
    while (i<text.length)
    {
        let [type, [start, end]] = eat_token(i);
        if (type!='whitespace')
            yield text.slice(start, end);
        i = end;
    }
}
