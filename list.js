let E = module.exports

E.zip = function*(...inputs){
    let i = 0
    while (true)
    {
        let out = []
        for (let c of inputs)
        {
            let v = c[i]
            if (v===undefined)
                return
            out.push(v)
        }
        yield out
        i++
    }
}
