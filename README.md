# Semi-correct markov chain experiments

## Install

```
git clone <repo>
```

## How to use

```
node states.js 3 text.txt > text_model.json
node generate.js 3 text_model.json
```

## Next steps

+ Pick the next token probabilistically instead of always picking the top
  choice
c Add a loop guard that lowers the probability of recently picked tokens
  (Canceled because weighted next tokens solve the loop problem)
- Improve language models with way more books
- Make the discord bot handle generic prompts better
- Test RNN language model
- Test LSTM language model
- Test transformer language model

## Open questions

- Can markov chains be used for capacity planning problems?

## Inspiration

Things like https://github.com/Deimos/SubredditSimulator
